Jakarta Surface Show
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title></title>
    <!--[if mso]>
        <noscript>
          <xml>
            <o:OfficeDocumentSettings>
              <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
          </xml>
        </noscript>
        <![endif]-->
</head>

<body style="margin:0;padding:0;">
    <table role="presentation"
        style="width:100%; border-collapse:collapse; border:0; border-spacing:0; background:#f1f0f0;">
        <tr>
            <td align="center" style="padding:0;">
                <table role="presentation"
                    style="width:560px; border-collapse:collapse; border:1px solid #e5e1d8; border-spacing:0;text-align:left;">
                    <tr>
                        <td align="center" style="padding:20px 0 20px 0; background-color:#ed1c25;">
                            <img src="{{ asset('assets/images/logo-white.png') }}" alt="" width="200"
                                style="height:auto;display:block;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:36px 30px 42px 30px; background-color: #ffffff;">
                            <table role="presentation"
                                style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">

                                <tr>
                                    <td style="padding:0 0 8px 0;color:#000000;">
                                        <p
                                            style="font-size:14px;margin:0 0 20px 0;font-family:futura-medium,sans-serif; font-weight:700;">
                                            Dear {{ $pnr->name ?? '' }} {{ $pnr->surname ?? '' }}
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            We would like to thank you for your interest in Jakarta Surface Show Fair.
                                            As TG Expo, the global brand of Turkish Fair Industry, we are pleased to
                                            welcome you to Jakarta Surface Show Fair.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            We present below the link to your personalized Fair Entry Card and your PNR
                                            number.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            To save time during your entry, please print out your entry card. We wish
                                            you a productive event...
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 !important; font-family:futura-medium,sans-serif; color:#000000; font-weight:700; text-align:justify">
                                            Your Online Badge;
                                        </p>
                                        <a href="{{ url('/print/eticket/' . $pnr->ticket_no) }}" target="_blank"
                                            style="text-decoration: underline; color: #blue; font-weight:500; font-family:futura-medium,sans-serif;"
                                            rel="noopener noreferrer">
                                            {{ url('/print/eticket/' . $pnr->ticket_no) }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 !important; font-family:futura-medium,sans-serif; color:#000000; font-weight:700; text-align:justify">
                                            Your PNR Number;
                                        </p>
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            {{ $pnr->pnr }}
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:1px 0 1px 0; width:100%; background:#000000; margin: 4px 0px;">

                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:8px 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 !important; font-family:futura-medium,sans-serif; color:#000000; font-weight:700; text-align:justify">
                                            Important Information
                                        </p>
                                        <ol type="1" style="font-family:futura-medium,sans-serif; padding:0 1rem">
                                            <li>Please bring a printout of your badge for quick fair entry.</li>
                                            <li>You can receive your invoice by indicating your registration number at
                                                the fair entrance.</li>
                                            <li>For any questions regarding e-tickets and registration, you can send an
                                                email to <a href="mailto:marketing@tgexpo.com">marketing@tgexpo.com.</a>
                                            </li>
                                        </ol>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            We wish you a productive and successful fair experience.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify">
                                            Best Regards,
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 8px 0;">
                                        <p
                                            style="font-size:14px;margin:0 0 16px 0;font-family:futura-medium,sans-serif; color:#000000; text-align:justify; font-weight: 600;">
                                            Jakarta Surface Show Team
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding:2px 0px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 16px 0; background-color:#ed1c25; text-align: center;">
                            <a target="_blank" href="https://www.tgexpo.com"
                                style="text-align: center; color:blue; font-family:futura-medium,sans-serif;">
                                www.tgexpo.com
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
