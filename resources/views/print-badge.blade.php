<html>

<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">

    <style>
        html {
            margin: 0 !important;
            font-family: 'Roboto', sans-serif;
        }

        body {
            margin: 0 !important;
            font-family: 'Roboto', sans-serif;
        }

        .wrapper-badges {
            font-family: 'Roboto', sans-serif;
            margin: 0 !important;
            position: relative;
        }

        @media print {
            .wrapper-badges {
                page-break-after: always;
            }
        }
    </style>
   
</head>

<body>
    @foreach ($pnrs as $pnr)
        {!! $pnr->specimentDetail->style_bulk !!}
        <div class="wrapper-badges"
            style="background: url('{{ '/assets/images/speciment/' . $pnr->specimentDetail->image }}') no-repeat;
         background-size: cover;
          width: 9.5cm !important;
          height: 13.3cm !important;">
            <div class="wrapper-name">
                <div class="owner">
                    {{ $pnr->name }} {{ $pnr->surname }}
                </div>
                <div class="job_title">{{ $pnr->title }}</div>
                <div class="company">{{ $pnr->company }}</div>
                <img class="qrcode" src="data:image/png;base64,{!! DNS2D::getBarcodePNG($pnr->code, 'QRCODE', 4, 4) !!}" alt="">
            </div>
        </div>
    @endforeach
    <script>
        window.print()
    </script>
</body>



</html>
