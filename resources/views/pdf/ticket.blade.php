<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ONLİNE BİLET</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet" />

    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact !important;
                /*Chrome, Safari */
                color-adjust: exact !important;
                /*Firefox*/
            }

            html,
            body {
                width: 210mm;
                height: 297mm;
                padding: 0 !important;
                background-color: #fff !important;
            }

            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        * {
            margin: 0;
            padding: 0;
            list-style-type: none;
            border: 0;
            text-decoration: none;
            box-sizing: border-box;
            line-height: 21px;
        }

        body {
            font-family: "Roboto", sans-serif;
            font-size: 15px;
            background-color: #d6d6d6;
            padding-top: 30px;
            padding-bottom: 30px;
        }

        .page {
            width: 210mm;
            min-height: 297mm;
            margin: 0 auto;
            padding: 0;
            border: 1px #d3d3d3 solid;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        #main {
            padding: 7mm;
            border: 1px white solid;
            height: 297mm;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .logo img {
            max-width: 280px;
        }

        .header-info span {
            display: block;
        }

        .fair-info {
            display: flex;
            margin-top: 40px;
        }

        .fair-image img {
            width: 350px;
            height: 250px;
        }

        .fair-text {
            margin-right: 25px;
        }

        .fair-text h4 {
            font-weight: bold;
        }

        .fair-text p.dear {
            margin-bottom: 20px;
            margin-top: 20px;
        }

        .fair-text p.btn {
            margin-bottom: 10px;
        }

        .fair-text span {
            background-color: #3f3f3f;
            padding: 5px 15px 5px 15px;
            color: #fff;
            display: block;
        }

        .ticket-info {
            display: flex;
            margin-top: 75px;
        }

        .ticket-text {
            width: 50%;
        }

        .ticket-text h4.title {
            font-weight: 900;
            margin-bottom: 20px;
        }

        .ticket-text ul.start li {
            margin-bottom: 20px;
        }

        .ticket-text ul.start li::before {
            content: "■ ";
            font-size: 1.25em;
        }

        .ticket-text .fair-link {
            margin-top: 50px;
            margin-bottom: 50px;
            text-align: center;
        }

        .ticket-text .fair-link h4 {
            font-weight: normal;
        }

        .ticket-text .fair-link h4.link {
            font-weight: bold;
        }

        .ticket-text .end {
            font-size: 11px;
            font-weight: bold;
            text-align: center;
        }

        .ticket-text .end::after {
            margin-top: 8px;
            content: "";
            display: inline-block;
            width: 15px;
            height: 100px;
            background-color: #3f3f3f;
        }

        .ticket-image {
            width: 372px;
            height: 530px;
            background-image: url("https://i.imgyukle.com/2020/12/02/YPL8zY.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
            text-align: center;
        }



        .ticket-image .text {
            margin-top: 210px;
            padding-left: 30px;
            text-transform: uppercase;
        }

        .ticket-image .text>h3 {
            font-size: 22px;
            font-weight: 900;
            margin-bottom: 10px;
        }

        .ticket-image .text>h4 {
            font-weight: normal;
            font-size: 18px;
            margin-bottom: 25px;
        }

        .centerqr {
            width: 100%;
            display: flex;
            justify-content: center;
        }
    </style>
</head>

<body>
    <div class="page">
        <div id="main">
            <div class="header">
                <div class="logo">
                    <img src="{{ asset('/assets/images/logo-footer.png') }}" alt="" />
                </div>
                <div class="header-info">
                    <span><b>Pnr No :</b> {{ $pnr->pnr }} </span>
                    <span><b>Date :</b> {{ $pnr->created_at }}</span>
                </div>
                <div class="barcode" style="text-align:center;">
                    {!! DNS1D::getBarcodeHTML($pnr->code, 'PHARMA') !!}
                    {{ $pnr->code }}
                    {{-- <img src="data:image/png," /> --}}
                </div>
            </div>

            <div class="fair-info">
                <div class="fair-text">
                    <h4>Dear {{ $pnr->name }} {{ $pnr->surname }}</h4>
                    <p class="dear">
                        We appreciate your kind interest towards Jakarta Surface Show Exhibiton. Kindly remind you to
                        take your entrance card printed in order to save time during your entrance. We wish you a
                        productive event.
                        we wish an event.
                    </p>
                    <h3>6-8 June 2024</h3>
                    <p class="btn">
                        Grand Ballroom - JI Expo Convention Centre and Theatre <br>
                        Enter from Gate 9 JI Expo to access Convention Centre
                    </p>
                    <span> VISITING HOURS : </p>

                        <p>Thursday - Friday, 6 - 7 June : 10:00 – 18:00</p>

                        <p>Saturday, 8 June : 10:00&nbsp;– 16:00</p>

                        <p>
                    </span>
                </div>
                <div class="fair-image">
                    <img src="{{ asset('/assets/images/jss.jpg') }}" alt="" />
                </div>
            </div>

            <div class="ticket-info">
                <div class="ticket-text">
                    <h4 class="title">Important Information</h4>
                    <ul class="start">
                        <li>For free entrance, please print your your badge and bring it with you.</li>
                        <li>For keeping the barcode visible, fold along the dotted lines or cut it out.</li>
                        <li>You could find badge lanyard on registration desks at the Exhibition area for free.</li>
                    </ul>
                    <div class="fair-link">
                        <h4>For Exhibition events and detailed information</h4>
                        <h4 class="link">http://www.jakartasurfaceshow.com/en/homepage/</h4>
                    </div>
                    <div class="end">
                        <ul>
                            <li>The organizer has right to verify badge via ID.</li>
                            <li>Thank you for interest in the exhibition.</li>
                        </ul>
                    </div>
                </div>
                <div class="ticket-image">
                    <div class="text">
                        <h3>Jakarta Surface Show</h3>
                        <h4 style="margin-bottom: 0px;">{{ $pnr->name }} {{ $pnr->surname }}</h4>
                        <h4>{{ $pnr->company }}</h4>
                        <div class="qrcode">
                            <h3>TGEXPO</h3>
                            <p>Turkey</p>
                            <div class="centerqr" id="qrcode">
                                {!! DNS2D::getBarcodeSVG($pnr->code, 'QRCODE', 5, 5) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script>
    setTimeout(function() {
        window.print()
        // .print()

    }, 1000);
</script>

</html>
