import { FaFacebookF, FaTwitter, FaInstagram, FaLinkedinIn } from "react-icons/fa6";
import { TiSocialYoutubeCircular } from "react-icons/ti";
import { IoMenu } from "react-icons/io5";
import { MdClose } from "react-icons/md";
import { Fragment, useState } from "react";
import { Transition, TransitionChild } from "@headlessui/react";
import { Head } from "@inertiajs/react";
export default function Navigation({ title = '', children }) {
    const menus = [
        {
            name: "HOMEPAGE",
            url: "https://onlinebilet.tgexpo.com/en",
        },
        {
            name: "HOW DOES IT WORK",
            url: "https://onlinebilet.tgexpo.com/en/page/how-it-works",
        },
        {
            name: "HOW TO GET THERE",
            url: "https://onlinebilet.tgexpo.com/en/page/how-to-get-there",
        },
        {
            name: "CONTACT",
            url: "https://onlinebilet.tgexpo.com/en/page/contact",
        },
        {
            name: "TGEXPO",
            url: "https://tgexpo.com/",
        },
    ]
    const [show, setShow] = useState(false)
    return (<>
        <Head title={title} />
        <div className="fixed top-0 left-0 z-10 bg-white w-full shadow px-2">
            <div className="container max-w-6xl mx-auto flex justify-between py-2 px-2 md:px-2 items-center">
                <div className="px-2 py-1">
                    <img className="" src="/assets/images/logo-footer.png" />
                </div>
                <div className="hidden md:flex justify-end divide-x-2">
                    {menus.map((menu, i) => <div key={i} className="text-center cursor-pointer py-2 hover:text-white hover:bg-red-jss">
                        <a href={menu.url} target="_blank" className="px-2 md:px-4">
                            {menu.name}
                        </a>
                    </div>)}
                </div>
                <div className="block md:hidden">
                    <div className=" bg-red-jss p-1 cursor-pointer" onClick={() => setShow(true)}>
                        <IoMenu className="text-white w-10 h-10" />
                    </div>
                </div>
                <Transition
                    show={show}
                    className={'fixed md:hidden flex left-0 top-0 w-full h-screen z-18 bg-[#b3202a] items-center'}
                    enter="transform transition ease-in-out duration-500 sm:duration-500"
                    enterFrom="-translate-y-full"
                    enterTo="translate-y-0"
                    leave="transform transition ease-in-out duration-500 sm:duration-500"
                    leaveFrom="translate-y-0"
                    leaveTo="-translate-y-full "
                >
                    <div>
                        <div className="absolute top-4 right-4" onClick={() => setShow(false)}>
                            <MdClose className="w-10 h-10 text-white" />
                        </div>
                        <div className="grid w-full">
                            {menus.map((menu, i) => <a href={menu.url} target="_blank" key={i} className="text-center cursor-pointer py-2 text-white text-2xl font-semibold hover:bg-red-jss">
                                <div className="px-2 md:px-4">
                                    {menu.name}
                                </div>
                            </a>)}
                        </div>
                    </div>

                </Transition>
            </div>
        </div>
        <div className="grid min-h-screen content-between gap-4">
            <div className="w-full pt-24 md:pt-32">
                <div className="container px-2 max-w-6xl mx-auto">
                    {children}
                </div>
            </div>
            <div className="w-full">
                <div className="grid">
                    <div className="bg-gray-footer grid justify-center md:flex md:justify-between gap-2 py-4 px-6">
                        <img src="/assets/images/logo-footer.png" />
                        <div className="flex justify-center md:items-center gap-1 my-4 md:my-0">
                            <a className="rounded-full bg-gray-socmed p-1" href="https://www.facebook.com/TgExpoFuarcilik" target="_blank" rel="noopener noreferrer">
                                <FaFacebookF className="text-white md:h-5 md:w-5" />
                            </a>
                            <a className="rounded-full  bg-gray-socmed p-1" href="https://twitter.com/tgexpo_official" target="_blank" rel="noopener noreferrer">
                                <FaTwitter className="text-white md:h-5 md:w-5" />
                            </a>
                            <a className="rounded-full  bg-gray-socmed p-1" href="https://www.instagram.com/tgexpofuarcilik/" target="_blank" rel="noopener noreferrer">
                                <FaInstagram className="text-white md:h-5 md:w-5" />
                            </a>
                            <a className="rounded-full  bg-gray-socmed p-1" href="https://onlinebilet.tgexpo.com/en/linkedin.com/company/tgexpo/" target="_blank" rel="noopener noreferrer">
                                <FaLinkedinIn className="text-white md:h-5 md:w-5" />
                            </a>
                            <a className="rounded-full  bg-gray-socmed p-1 " href="https://www.youtube.com/c/TgexpoUluslararasiFuarcilik" target="_blank" rel="noopener noreferrer">
                                <TiSocialYoutubeCircular className="text-white md:h-5 md:w-5" />
                            </a>
                        </div>
                    </div>
                    <div className="bg-black flex gap-2 py-4 px-6 justify-between">
                        <p className="text-gray-text-footer text-center grow">
                            All Rights reserved.
                        </p>
                        <div className="flex justify-center grow gap-2">

                            <a className="text-gray-text-footer" href="https://onlinebilet.tgexpo.com/en/page/privacy-policy" target="_blank" rel="noopener noreferrer">
                                Privacy Policy
                            </a>
                            <a className="text-gray-text-footer" href="https://onlinebilet.tgexpo.com/en/page/distance-selling-contract" target="_blank" rel="noopener noreferrer">
                                Distance Selling Contract
                            </a>
                            <a className="text-gray-text-footer" href="https://onlinebilet.tgexpo.com/en/page/refund-conditions" target="_blank" rel="noopener noreferrer">
                                Refund Conditions
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>)
};
