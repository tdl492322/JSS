import Guest from "@/Layouts/GuestLayout";
import TextInput from "@/Components/TextInput"
import { Head } from "@inertiajs/react"
import axios from "axios"
import { useState } from "react"
export default function Scan(props) {
    const success = new Audio(window.location.origin + "/assets/audio/success.mp3");
    const initiateData = {
        location: props.location,
        code: '',
        speciment: "",
        name: '',
        surname: "",
        title: '',
        company: '',
        message: '',
        status_code: "",
    }
    const [data, setData] = useState(initiateData)
    console.log(data);
    const submit = (e) => {
        e.preventDefault();
        axios.post(window.location.origin + '/scanner', {
            code: data.code,
            location: data.location,
        }).then((res) => {
            // console.log(res);
            if (res.data.status_code == 200) {
                success.play();
                setData({
                    ...data,

                    code: res.data.code,
                    name: res.data.name,
                    surname: res.data.surname,
                    title: res.data.title,
                    company: res.data.company,
                    message: res.data.message,
                    speciment: res.data.speciment,
                    status_code: res.data.status_code,
                })
            } else {
                setData({
                    ...data,
                    location: props.location,
                    code: '',
                    speciment: "",
                    name: '',
                    surname: "",
                    title: '',
                    company: '',
                    status_code: res.data.status_code,
                    message: res.data.message,
                })
            }
        }).catch((err) => {
            alert(err);
        }).finally(() => {
            setTimeout(() => {
                success.pause();
                success.currentTime = 0;
                setData(initiateData)
            }, 2000);
        })
    }
    return (<Guest >
        <form onSubmit={submit} className="grid grid-cols-12 gap-2">
            <div className="col-span-12">
                <h1 className="text-red-jss text-3xl text-center font-bold">Welcome to Jakarta Surface Show</h1>
            </div>
            {data.status_code == 200 ?
                <div className="col-span-12 grid relative">
                    <div className="font-bold text-lg uppercase text-center"
                    // style={{
                    //     position: "absolute",
                    //     width: "100%",
                    //     top: "51%",
                    //     textAlign: "center"
                    // }}
                    >{data.name} {data.surname}
                        <br />
                        <span className="text-sm font-semibold">
                            {data.title}
                        </span>
                        <br />
                        <span className="text-sm font-semibold">
                            {data.company}
                        </span>
                    </div>
                    <div className="text-black text-sm text-center"
                    // style={{
                    //     position: "absolute",
                    //     left: '47.5%',
                    //     top: "85%",
                    //     textAlign: "center"
                    // }}
                    >{data.code}</div>
                    <div className="text-black text-sm text-center">
                        {data.location}
                    </div>
                </div> : (data.status_code === 404 ? <div className="col-span-12 grid">
                    <div className="text-lg bg-red-600 rounded shadow text-center text-black py-4 font-black">{data.message}</div>
                </div> : null)}
            <div className="col-span-12 opacity-0">
                <TextInput
                    name={'code'}
                    className={'w-full'}
                    value={data.code}
                    onChange={(e) => setData({ ...data, code: e.target.value })}
                    isFocused
                    required
                />
            </div>
        </form>
    </Guest>)
};
