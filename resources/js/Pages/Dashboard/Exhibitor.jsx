import { useState } from "react";
import DashboardLayout from "@/Layouts/DashboardLayout";
import useSWR from "swr";
import DataGrid from "@/Components/DataGrid";
import { IoMdInformationCircleOutline } from "react-icons/io";
import { format } from "date-fns";
import { Link, router } from "@inertiajs/react";
import axios from "axios";
export default function Exhibitor(props) {
    const fetcher = (...args) => fetch(...args).then(res => res.json())
    const { data, mutate, error, isLoading } = useSWR(props.swrUrl, fetcher)
    const columns = [
        { name: 'id', header: 'Id', defaultWidth: 42, defaultVisible: false },
        { name: 'no', header: '#', defaultWidth: 42, defaultVisible: true },
        { name: 'speciment', sortable: true, header: 'Speciment', defaultWidth: 152, },
        { name: 'source', sortable: true, header: 'Source', defaultWidth: 152, },
        {
            name: 'ticket_no',
            sortable: true,
            header: 'Ticket No',
            defaultWidth: 152,
        },
        {
            name: 'name',
            sortable: true,
            header: 'Name',
            defaultWidth: 152,
        },
        {
            name: 'surname',
            sortable: true,
            header: 'surname',
            defaultWidth: 152,
        },
        {
            name: 'country',
            sortable: true,
            header: 'Country',
            defaultWidth: 128,
        },
        {
            name: 'city',
            sortable: true,
            header: 'City',
            defaultWidth: 128,
        },
        {
            name: 'region_code',
            sortable: true,
            header: 'Region Code',
            defaultWidth: 72,
        },
        {
            name: 'phone',
            sortable: true,
            header: 'Phone',
            defaultWidth: 128,
        },
        {
            name: 'email',
            sortable: true,
            header: 'Email',
            defaultWidth: 152,
        },
        {
            name: 'company',
            sortable: true,
            header: 'Company',
            defaultWidth: 152,
        },
        {
            name: 'title',
            sortable: true,
            header: 'Title',
            defaultWidth: 152,
        },
        {
            name: 'fair_name',
            sortable: true,
            header: 'Fair Name',
            defaultWidth: 128,
        },
        {
            name: 'pnr',
            sortable: true,
            header: 'PNR Number',
            defaultWidth: 128,
        },
        {
            name: 'code',
            sortable: true,
            header: 'Barcode Number',
            defaultWidth: 128,
        },
        {
            name: 'submit_date',
            sortable: true,
            header: 'Submit Date',
            defaultWidth: 256,
        },
        {
            name: 'action',
            sortable: true,
            header: 'Action',
            defaultWidth: 256,
            render: ({ value }) => (<div className="flex space-x-2 justify-start">
                <Link as="a" href="/" className="bg-red-jss py-1 px-4 text-center text-white rounded shadow">Archive</Link>
            </div>),
        }
    ]
    const filterValue = [
        { name: 'speciment', operator: 'contains', type: 'string', value: '' },
        { name: 'source', operator: 'contains', type: 'string', value: '' },
        { name: 'ticket_no', operator: 'contains', type: 'string', value: '' },
        { name: 'name', operator: 'contains', type: 'string', value: '' },
        { name: 'surname', operator: 'contains', type: 'string', value: '' },
        { name: 'country', operator: 'contains', type: 'string', value: '' },
        { name: 'city', operator: 'contains', type: 'string', value: '' },
        { name: 'region_code', operator: 'contains', type: 'string', value: '' },
        { name: 'phone', operator: 'contains', type: 'string', value: '' },
        { name: 'email', operator: 'contains', type: 'string', value: '' },
        { name: 'company', operator: 'contains', type: 'string', value: '' },
        { name: 'title', operator: 'contains', type: 'string', value: '' },
        { name: 'fair_name', operator: 'contains', type: 'string', value: '' },
        { name: 'price', operator: 'contains', type: 'string', value: '' },
        { name: 'pnr', operator: 'contains', type: 'string', value: '' },
        { name: 'code', operator: 'contains', type: 'string', value: '' },
        { name: 'submit_date', operator: 'contains', type: 'string', value: '' },
    ];
    let dataSource = [];
    if (data) {
        dataSource = data.registrant.map((regist, idx) => ({
            id: regist.id,
            no: Number(idx + 1),
            speciment: regist.speciment_detail?.label,
            source: regist.source,
            ticket_no: regist.ticket_no,
            name: regist.name,
            surname: regist.surname,
            country: regist.country,
            city: regist.city,
            region_code: regist.region_code,
            phone: regist.phone,
            email: regist.email,
            company: regist.company,
            title: regist.title,
            fair_name: regist.fair_name,
            price: regist.price,
            pnr: regist.pnr,
            code: regist.code,
            submit_date: format(regist.created_at, "yyyy-MM-dd HH:ii"),
            action: regist,
        }))
    }

    const [dataSelected, setDataSelected] = useState([]);
    const handleMultiItems = (action) => {
        if (action === 'Print') {
            axios.post(window.location.origin + '/dashboard/mass-action', {
                action: action,
                selected: dataSelected
            })
                .then((res) => { window.open(res.data.url, "_blank") })
                .catch((err) => { console.log(err); })
        }
    }
    const uploadData = (e) => {
        if (e.target.files[0]) {
            router.post('/dashboard/upload-template', {
                template: e.target.files[0]
            }, {
                onSuccess: () => mutate
            })
        } else {
            alert('Please upload Template File')
        }

    }
    return (<DashboardLayout title={'Exhibitor'}>
        <div className="grid grid-cols-12 gap-2">
            <div className="col-span-12 flex justify-start gap-4">
                {dataSelected.length > 0 ? <>
                    <div className="bg-yellow-200 py-1 px-4 rounded shadow text-black cursor-pointer" onClick={() => handleMultiItems('Print')}>Print {dataSelected.length} Items</div>
                </> : null}
                <a className="bg-blue-500 text-white py-1 px-2 rounded shadow cursor-pointer" href="/download-template-badge">Download Template</a>
                <label htmlFor="upload-template" className="bg-red-jss py-1 px-2 rounded shadow cursor-pointer text-white">Upload Data</label>
                <input
                    type="file"
                    id="upload-template"
                    className="hidden" onChange={uploadData}
                />
                <Link as="a" className=" bg-indigo-600 py-1 px-2 rounded shadow cursor-pointer text-white" href="/dashboard/add-new?speciment=EXHIBITOR">Add New</Link>
            </div>
            {error
                ? <div className="col-span-12 text-lg text-center flex gap-2 items-center">
                    <IoMdInformationCircleOutline className="h-8 w-8 text-red-600" />
                    Failed to load
                    <div className="btn bg-red-prodigy border-none btn-sm hover:bg-red-500 text-white" onClick={() => { window.location.reload() }}>Reload</div>
                </div>
                : !data
                    ? <div className="col-span-12 flex gap-2">
                        Loading
                    </div>
                    :
                    <div className="col-span-12">
                        <DataGrid
                            idProperty={'id'}
                            columns={columns}
                            dataSource={dataSource}
                            pagination={true}
                            filterValue={filterValue}
                            rowHeight={120}
                            maxRowHeight={400}
                            handleSelected={(selected) => setDataSelected(selected)}
                        />
                    </div>}
        </div>
    </DashboardLayout>)
};
