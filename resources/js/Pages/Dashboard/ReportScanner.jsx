import { useState } from "react";
import DashboardLayout from "@/Layouts/DashboardLayout";
import useSWR from "swr";
import DataGrid from "@/Components/DataGrid";
import { IoMdInformationCircleOutline } from "react-icons/io";
import Select from "@/Components/Select";
export default function ReportScanner(props) {
    const [date, setDate] = useState("2024-06-06")
    const fetcher = (...args) => fetch(...args).then(res => res.json())
    const { data, mutate, error, isLoading } = useSWR(props.swrUrl + "?date=" + date, fetcher)
    const columns = [
        { name: 'id', header: 'Id', defaultWidth: 42, defaultVisible: false },
        { name: 'no', header: '#', defaultWidth: 42, defaultVisible: true },
        { name: 'code', sortable: true, header: 'Code', defaultWidth: 152, },
        { name: 'speciment', sortable: true, header: 'Speciment', defaultWidth: 152, },
        { name: 'name', sortable: true, header: 'Name', defaultWidth: 152, },
        { name: 'title', sortable: true, header: 'Job Title', defaultWidth: 152, },
        { name: 'company', sortable: true, header: 'Company', defaultWidth: 152, },
        { name: 'country', sortable: true, header: 'Country', defaultWidth: 152, },
        { name: 'scan_in', sortable: true, header: 'Time Scan', defaultWidth: 152, },
    ]
    const filterValue = [
        { name: 'code', operator: 'contains', type: 'string', value: '' },
        { name: 'speciment', operator: 'contains', type: 'string', value: '' },
        { name: 'name', operator: 'contains', type: 'string', value: '' },
        { name: 'title', operator: 'contains', type: 'string', value: '' },
        { name: 'company', operator: 'contains', type: 'string', value: '' },
        { name: 'country', operator: 'contains', type: 'string', value: '' },
        { name: 'scan_in', operator: 'contains', type: 'string', value: '' },
    ];
    let dataSource = [];
    if (data) {
        dataSource = data.scans.map((scan, idx) => ({
            id: scan.id,
            no: Number(idx + 1),
            code: scan.code,
            speciment: scan.speciment,
            name: scan.detail_scanned.name + ' ' + scan.detail_scanned.surname,
            title: scan.detail_scanned.title,
            company: scan.detail_scanned.company,
            country: scan.detail_scanned.country,
            scan_in: scan.scan_in,
            location: scan.location,
        }))
    }


    return (<DashboardLayout title={'Report Scanner'}>
        <div className="grid grid-cols-12 gap-2">
            <div className="col-span-12 flex justify-start gap-4">
                <Select
                    id={'date'}
                    name={'date'}
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                >
                    <option value={""}>Select Date</option>
                    <option value={"2024-06-06"}>2024-06-06</option>
                    <option value={"2024-06-07"}>2024-06-07</option>
                    <option value={"2024-06-08"}>2024-06-08</option>
                </Select>
            </div>
            {error
                ? <div className="col-span-12 text-lg text-center flex gap-2 items-center">
                    <IoMdInformationCircleOutline className="h-8 w-8 text-red-600" />
                    Failed to load
                    <div className="btn bg-red-jss border-none btn-sm hover:bg-red-500 text-white" onClick={() => { window.location.reload() }}>Reload</div>
                </div>
                : !data
                    ? <div className="col-span-12 flex gap-2">
                        Loading
                    </div>
                    :
                    <div className="col-span-12">
                        <DataGrid
                            idProperty={'id'}
                            columns={columns}
                            dataSource={dataSource}
                            pagination={true}
                            filterValue={filterValue}
                            rowHeight={120}
                            maxRowHeight={400}
                            handleSelected={(selected) => setDataSelected(selected)}
                        />
                    </div>}
        </div>
    </DashboardLayout>)
};
