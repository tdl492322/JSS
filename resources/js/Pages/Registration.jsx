import Checkbox from '@/Components/Checkbox';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import Navigation from '@/Layouts/Navigation';
import { useForm } from '@inertiajs/react';
import { FaCalendarAlt } from "react-icons/fa";
import { Country, State } from 'country-state-city'
import Select from '@/Components/Select';
export default function Registration(props) {
    const { data, setData, post, errors, processing, reset } = useForm({
        name: '',
        surname: '',
        iso_code: 'US',
        country: 'United States',
        city: 'Alabama',
        region_code: '',
        phone_no: '',
        email_address: '',
        company_name: '',
        title: '',
        confirm: '',
    })
    const submit = (e) => {
        e.preventDefault()
        post('/registration');
    }
    return (
        <Navigation title='PARTICIPATION FORM' >
            <div className='w-full grid grid-cols-12 gap-2'>
                <div className="col-span-12">
                    <h1 className='text-red-jss text-xl md:text-2xl uppercase roboto-bold'> Online Ticket</h1>
                </div>
            </div>
            <form onSubmit={submit} className='bg-white w-full grid grid-cols-12 gap-2 p-4 md:p-6'>
                <div className='col-span-12 border border-red-jss grid grid-cols-11 gap-2 p-4'>
                    <div className="col-span-11 md:col-span-7 grid gap-2">
                        <h2 className='text-red-jss text-lg md:text-xl roboto-bold'>Jakarta Surface Show</h2>
                        <p className=' roboto-medium'>
                            International Natural Stone and Coverings Show
                        </p>
                        <div className="flex gap-2 items-center">
                            <div className='bg-red-jss p-1.5'>
                                <FaCalendarAlt className='text-white h-6 w-5' />
                            </div>
                            <div className=' font-black text-gray-700 text-lg'>
                                6-8 June 2024
                            </div>
                        </div>
                        <div className='flex items-center justify-center w-full bg-green-jss text-white'>
                            FREE
                        </div>
                    </div>
                    <div className='hidden md:block col-span-11 md:col-span-2'></div>
                    <div className='hidden md:block col-span-11 md:col-span-2'>
                        <img src={'/assets/images/jss.jpg'} />
                    </div>

                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'name'} value={'Name'} required />
                    <TextInput
                        id={'name'}
                        name={'name'}
                        className={'w-full mt-1'}
                        value={data.name}
                        placeholder={'Name'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <InputError className='mt-1' message={errors.name} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'surname'} value={'Surname'} required />
                    <TextInput
                        id={'surname'}
                        name={'surname'}
                        className={'w-full mt-1'}
                        value={data.surname}
                        placeholder={'Surname'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <InputError className='mt-1' message={errors.surname} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'country'} value={'Country'} required />
                    <Select
                        name={'country'}
                        className={'w-full'}
                        value={data.country != '' ? data.country : "United States"}
                        handleChange={e => {
                            setData({
                                ...data,
                                [e.target.name]: e.target.value,
                                iso_code: e.target.selectedOptions[0].getAttribute('isocode'),
                                city: (State.getStatesOfCountry(e.target.selectedOptions[0].getAttribute('isocode')).length > 0
                                    ? State.getStatesOfCountry(e.target.selectedOptions[0].getAttribute('isocode'))[0].name
                                    : e.target.value),
                                region_code: Country.getCountryByCode(e.target.selectedOptions[0].getAttribute('isocode')).phonecode,
                            })
                        }}
                        required>
                        {Country.getAllCountries().map((country, i) => <option key={i} isocode={country.isoCode} value={country.name}>{country.name}</option>)}
                    </Select>
                    <InputError className='mt-1' message={errors.country} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'city'} value={'City'} required />
                    <Select
                        name={'city'}
                        className={'w-full'}
                        value={data.city != "" ? data.city : ""}
                        handleChange={e => setData(e.target.name, e.target.value)}
                        required
                    >
                        {State.getStatesOfCountry(data.iso_code).length > 0 ? State.getStatesOfCountry(data.iso_code).map((state, i) =>
                            <option key={i} value={state.name}>{state.name}</option>
                        ) : <option value={data.country}>{data.country}</option>}
                    </Select>
                    <InputError className='mt-1' message={errors.city} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'region_code'} value={'Region Code'} required />
                    <Select
                        name={'region_code'}
                        className={'w-full'}
                        value={data.region_code != '' ? Country.getAllCountries().find(el => el.phonecode === data.region_code).name : "United States"}

                        handleChange={e => {
                            setData({
                                ...data,
                                [e.target.name]: e.target.selectedOptions[0].getAttribute('regioncode'),
                            })
                        }}
                        required
                    >
                        {Country.getAllCountries().map((phonecode, i) => <option
                            key={i}
                            regioncode={phonecode.phonecode}
                            value={phonecode.name}>
                            {phonecode.name} (+{phonecode.phonecode})
                        </option>)}
                    </Select>
                    <InputError className='mt-1' message={errors.region_code} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'phone_no'} value={'Your Cell Phone'} required />
                    <TextInput
                        id={'phone_no'}
                        name={'phone_no'}
                        className={'w-full mt-1'}
                        value={data.phone_no}
                        placeholder={'Your Cell Phone'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <InputError className='mt-1' message={errors.phone_no} />
                </div>
                <div className="col-span-12 md:mb-4">
                    <InputLabel htmlFor={'email_address'} value={'Your Email Address'} required />
                    <TextInput
                        id={'email_address'}
                        type={'email'}
                        name={'email_address'}
                        className={'w-full mt-1'}
                        value={data.email_address}
                        placeholder={'Your Email Address'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <div className='my-2 text-sm font-bold text-red-jss'>(Important! Your entrance card will be sent to this e-mail address.)</div>
                    <InputError className='mt-1' message={errors.email_address} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'company_name'} value={'Company Name'} required />
                    <TextInput
                        id={'company_name'}
                        name={'company_name'}
                        className={'w-full mt-1'}
                        value={data.company_name}
                        placeholder={'Company Name'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <InputError className='mt-1' message={errors.company_name} />
                </div>
                <div className="col-span-12 md:col-span-6">
                    <InputLabel htmlFor={'title'} value={'Your Title'} required />
                    <TextInput
                        id={'title'}
                        name={'title'}
                        className={'w-full mt-1'}
                        value={data.title}
                        placeholder={'Your Title'}
                        onChange={e => setData(e.target.name, e.target.value)}
                        required
                    />
                    <InputError className='mt-1' message={errors.title} />
                </div>
                <div className='col-span-12 flex items-center gap-2 my-2'>
                    <Checkbox
                        name={'confirm'}
                        value={data.confirm}
                        onChange={(e) => setData(e.target.name, e.target.checked)}
                        required />
                    <a className=' text-blue-800 ' href="https://tgexpo.com/tr/6/Kurumsal/gizlilik-politikamiz" target="_blank" rel="noopener noreferrer">
                        Privacy Policy and information letter on the Use and Protection of Personal Data I read, I accept.
                    </a>
                </div>
                <div className='col-span-12 my-1'>
                    <PrimaryButton disabled={processing} className='!bg-black !text-base rounded-none w-full justify-center font-normal'>
                        REGISTER FREE INVITATION
                    </PrimaryButton>
                </div>
            </form>
        </Navigation>
    );
};
