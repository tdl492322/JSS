import Checkbox from '@/Components/Checkbox';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import Navigation from '@/Layouts/Navigation';
import { useForm } from '@inertiajs/react';
import { FaCalendarAlt } from "react-icons/fa";
import { Country, State } from 'country-state-city'
import Select from '@/Components/Select';

export default function PressRegistration(props) {
    const { data, setData, post, errors, processing, reset } = useForm({
        name: '',
        title: '',
        company_name: '',
        country: 'United States',
        confirm: '',
    })
    const submit = (e) => {
        e.preventDefault()
        post('/press-register');
    }
    return (<Navigation title="Press Registration">
        <div className='w-full grid grid-cols-12 gap-2'>
            <div className="col-span-12">
                <h1 className='text-red-jss text-xl md:text-2xl uppercase roboto-bold'> Online Ticket</h1>
            </div>
        </div>
        <form onSubmit={submit} className='bg-white w-full grid grid-cols-12 gap-2 p-4 md:p-6'>
            <div className='col-span-12 border border-red-jss grid grid-cols-11 gap-2 p-4'>
                <div className="col-span-11 md:col-span-7 grid gap-2">
                    <h2 className='text-red-jss text-lg md:text-xl roboto-bold'>Jakarta Surface Show</h2>
                    <p className=' roboto-medium'>
                        International Natural Stone and Coverings Show
                    </p>
                    <div className="flex gap-2 items-center">
                        <div className='bg-red-jss p-1.5'>
                            <FaCalendarAlt className='text-white h-6 w-5' />
                        </div>
                        <div className=' font-black text-gray-700 text-lg'>
                            6-8 June 2024
                        </div>
                    </div>
                    <div className='flex items-center justify-center w-full bg-green-jss text-white'>
                        FREE
                    </div>
                </div>
                <div className='hidden md:block col-span-11 md:col-span-2'></div>
                <div className='hidden md:block col-span-11 md:col-span-2'>
                    <img src={'/assets/images/jss.jpg'} />
                </div>

            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'name'} value={'Name'} required />
                <TextInput
                    id={'name'}
                    name={'name'}
                    className={'w-full mt-1'}
                    value={data.name}
                    placeholder={'Name'}
                    onChange={e => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className='mt-1' message={errors.name} />
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'country'} value={'Country'} required />
                <TextInput
                    id={'country'}
                    name={'country'}
                    className={'w-full mt-1'}
                    value={data.country}
                    placeholder={'Country'}
                    onChange={e => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className='mt-1' message={errors.country} />
            </div>

            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'company_name'} value={'Company Name'} required />
                <TextInput
                    id={'company_name'}
                    name={'company_name'}
                    className={'w-full mt-1'}
                    value={data.company_name}
                    placeholder={'Company Name'}
                    onChange={e => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className='mt-1' message={errors.company_name} />
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'title'} value={'Your Title'} required />
                <TextInput
                    id={'title'}
                    name={'title'}
                    className={'w-full mt-1'}
                    value={data.title}
                    placeholder={'Your Title'}
                    onChange={e => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className='mt-1' message={errors.title} />
            </div>
            <div className='col-span-12 flex items-center gap-2 my-2'>
                <Checkbox
                    name={'confirm'}
                    value={data.confirm}
                    onChange={(e) => setData(e.target.name, e.target.checked)}
                    required />
                <a className=' text-blue-800 ' href="https://tgexpo.com/tr/6/Kurumsal/gizlilik-politikamiz" target="_blank" rel="noopener noreferrer">
                    Privacy Policy and information letter on the Use and Protection of Personal Data I read, I accept.
                </a>
            </div>
            <div className='col-span-12 my-1'>
                <PrimaryButton disabled={processing} className='!bg-black !text-base rounded-none w-full justify-center font-normal'>
                    REGISTER
                </PrimaryButton>
            </div>
        </form>
    </Navigation>)
};
