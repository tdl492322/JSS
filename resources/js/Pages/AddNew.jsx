import InputError from "@/Components/InputError";
import InputLabel from "@/Components/InputLabel";
import PrimaryButton from "@/Components/PrimaryButton";
import Select from "@/Components/Select";
import TextInput from "@/Components/TextInput";
import DashboardLayout from "@/Layouts/DashboardLayout";
import { useForm } from "@inertiajs/react";

export default function AddNew(props) {
    const { data, setData, post, processing, reset, errors } = useForm({
        name: '',
        title: '',
        company: '',
        speciment: props.selectedSpeciment ? props.selectedSpeciment.label : '',
    });
    const submit = (e) => {
        e.preventDefault()
        post('/dashboard/create-new');
    }
    return (<DashboardLayout title="Add New Data">
        <form onSubmit={submit} className="grid grid-cols-12 gap-2">
            <div className="col-span-12">
                <InputLabel htmlFor={'speciment'} value={'Speciment'} />
                <Select
                    id={'speciment'}
                    name={'speciment'}
                    value={data.speciment}
                    className={'w-full my-2'}
                    onChange={(e) => setData(e.target.name, e.target.value)}
                    required
                >
                    <option value={''}>Select</option>
                    {props.speciments.map((spec, i) => <option value={spec.label}>{spec.label}</option>)}
                </Select>
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'name'} value={'Full Name'} />
                <TextInput
                    id={'name'}
                    name={'name'}
                    value={data.name}
                    className={'w-full my-2'}
                    placeholder={'Full Name'}
                    onChange={(e) => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className="mt-1" message={errors.name} />
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'title'} value={'Job Title'} />
                <TextInput
                    id={'title'}
                    name={'title'}
                    value={data.title}
                    className={'w-full my-2'}
                    placeholder={'Job Title'}
                    onChange={(e) => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className="mt-1" message={errors.title} />
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'company'} value={'Company'} />
                <TextInput
                    id={'company'}
                    name={'company'}
                    value={data.company}
                    className={'w-full my-2'}
                    placeholder={'Company'}
                    onChange={(e) => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className="mt-1" message={errors.company} />
            </div>
            <div className="col-span-12 md:col-span-6">
                <InputLabel htmlFor={'country'} value={'Country'} />
                <TextInput
                    id={'country'}
                    name={'country'}
                    value={data.country}
                    className={'w-full my-2'}
                    placeholder={'Country'}
                    onChange={(e) => setData(e.target.name, e.target.value)}
                    required
                />
                <InputError className="mt-1" message={errors.country} />
            </div>
            <div className="col-span-12">
                <PrimaryButton
                    className="w-full justify-center"
                    disabled={processing}>
                    Save and Print
                </PrimaryButton>
            </div>
        </form>
    </DashboardLayout>)
};
