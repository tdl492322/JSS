import Navigation from "@/Layouts/Navigation";
import { Link } from "@inertiajs/react";

export default function Success(props) {
    return (<Navigation title="Registration Success">
        <div className='w-full grid grid-cols-12 gap-2'>
            <div className="col-span-12">
                <h1 className='text-red-jss text-xl md:text-2xl uppercase roboto-bold'> Online Ticket</h1>
            </div>
        </div>
        <div className='bg-white w-full grid grid-cols-12 gap-2 p-4 md:p-6'>
            <div className="col-span-12 text-center">
                <h1 className="text-lg font-semibold text-red-jss mb-2">Congratulations!</h1>
                <p className="mb-2">Your process is done with success.</p>
                <p className="mb-2 text-red-jss font-semibold">PNR Number : <span className="text-black">{props.findPnr.pnr}</span></p>
                <p className="mb-2">(Please take note of the PNR number above)</p>
                <p className="mb-2">Your Fair Entrance Card which you declared <span className=" font-black">{props.findPnr.email}</span> is sent to.</p>
                <p className="mb-2 font-black">You can print out the link in your mail and get your Fair Entrance Card.</p>
                <p className="mb-2 font-black">Please take your fair entrance card with you not to lose time at fair gates.</p>
                <p className="mb-2">You can take your bill or fiche at the fair gate with declaring your registration number</p>
                <p className="mb-2">We appreciated for your attendance.</p>
            </div>
            <div className="col-span-12 flex justify-center items-center content-center mt-4">
                <Link href="/" className="bg-black w-full text-white py-1 px-2 text-center">
                    Main Page
                </Link>
            </div>
        </div>
    </Navigation>)
};
