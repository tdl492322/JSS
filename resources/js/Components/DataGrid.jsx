import React from 'react';
import ReactDataGrid from '@inovua/reactdatagrid-community';
import '@inovua/reactdatagrid-community/index.css';
import '@inovua/reactdatagrid-community/base.css';
import '@inovua/reactdatagrid-community/theme/default-light.css';
import ExportExcel from './Export/ExportExcel';
import { useState, useCallback } from 'react';
export default function DataGrid({ idProperty,
    columns,
    dataSource,
    filterValue,
    pagination = false,
    minHeight = 1000,
    handleSelected = () => { },
    ...props }) {
    const gridStyle = { height: 550, minHeight: minHeight }
    const [selected, setSelected] = useState()
    const [originDataSource, setOriginDataSource] = useState(dataSource)
    const onSelectionChange = useCallback(({ selected }) => {
        const keysObj = selected === true ? dataSource : selected
        setOriginDataSource(selected)
        handleSelected(Object.values(keysObj).map(value => value.id))
    }, [])
    return (
        <div className='grid grid-cols-12 gap-2'>
            <div className="col-span-12 grid gap-1">
                <div className="text-xs">
                    export as
                </div>
                <div className="flex gap-1">
                    <ExportExcel excelData={dataSource} />
                </div>
            </div>
            <div className="col-span-12">
                <ReactDataGrid
                    {...props}
                    checkboxColumn
                    selected={selected}
                    onSelectionChange={onSelectionChange}
                    idProperty={idProperty}
                    style={gridStyle}
                    columns={columns}
                    dataSource={dataSource}
                    defaultFilterValue={filterValue}
                    pagination={pagination}
                    defaultLimit={100}
                    enableColumnAutosize={true}
                />
            </div>
        </div>

    )
};
