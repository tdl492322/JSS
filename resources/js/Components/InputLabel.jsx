export default function InputLabel({ value, className = '', children, required = false, ...props }) {
    return (
        <label {...props} className={`block font-semibold text-lg text-black ` + className}>
            {value ? value : children} {required ? <sup className="text-red-jss font-semibold">*</sup> : null}
        </label>
    );
}
