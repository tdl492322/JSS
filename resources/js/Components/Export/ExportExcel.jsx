import * as FileSaver from 'file-saver';
import XLSX from 'xlsx';
export default function ExportExcel({ title, excelData, fileName = 'export-' + new Date().getTime() }) {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx'
    const exportToExcel = async () => {
        const ws = XLSX.utils.json_to_sheet(excelData);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] }
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' })
        const data = new Blob([excelBuffer], { type: fileType })
        FileSaver.saveAs(data, fileName + fileExtension)
    }
    return (<>
        <div className='py-1 px-2 rounded shadow cursor-pointer !normal-case border-none !text-xs bg-green-700 text-white hover:bg-green-300 hover:text-green-700' onClick={() => exportToExcel(fileName)}>
            Excel</div>
    </>)
};
