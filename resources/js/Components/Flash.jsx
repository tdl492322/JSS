import { usePage } from "@inertiajs/react";
import { useState } from "react";
import { Transition } from "@headlessui/react";
export default function Flash() {
    const [visible, setVisible] = useState(true)
    const flash = usePage().props.flash;
    if (flash) {
        if (flash.message) {
            return (<div className="fixed w-full z-14 bottom-2 left-1/2 -translate-x-1/2 ">
                <Transition show={visible} as={'div'}
                    enter="transform transition duration-[500ms]"
                    enterFrom="opacity-0 scale-50"
                    enterTo="opacity-100 scale-100"
                    leave="transform duration-200 transition ease-in-out"
                    leaveFrom="opacity-100 scale-100 "
                    leaveTo="opacity-0 scale-95">
                    <div className="mx-2 p-2 md:p-4 bg-green-400 text-black rounded shadow-md text-justify grid grid-cols-12 gap-2 ring ring-matte-brown ring-opacity-50 justify-between items-center">
                        <div className="col-span-11">
                            {flash.message}
                        </div>
                        <div className="col-span-1 justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" onClick={() => { setVisible(false) }} fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 cursor-pointer">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>

                    </div>
                </Transition>
            </div>
            )
        }
        if (flash.success) {
            return (<div className="fixed w-full z-14 bottom-2 left-1/2 -translate-x-1/2 ">
                <Transition show={visible} as={'div'}
                    enter="transform transition duration-[500ms]"
                    enterFrom="opacity-0 scale-50"
                    enterTo="opacity-100 scale-100"
                    leave="transform duration-200 transition ease-in-out"
                    leaveFrom="opacity-100 scale-100 "
                    leaveTo="opacity-0 scale-95"
                >
                    <div className="mx-2 p-2 md:p-4 bg-blue-300 text-black rounded shadow-md text-justify grid grid-cols-12 gap-2 ring ring-matte-brown ring-opacity-50 justify-between items-center">
                        <div className="col-span-11">
                            {flash.success}
                        </div>
                        <div className="col-span-1 justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" onClick={() => { setVisible(false) }} fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 cursor-pointer">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>

                    </div>
                </Transition>
            </div>
            )
        }
        if (flash.alert) {
            return (<div className="fixed w-full z-14 bottom-2 left-1/2 -translate-x-1/2 ">
                <Transition show={visible} as={'div'}
                    enter="transform transition duration-[500ms]"
                    enterFrom="opacity-0 scale-50"
                    enterTo="opacity-100 scale-100"
                    leave="transform duration-200 transition ease-in-out"
                    leaveFrom="opacity-100 scale-100 "
                    leaveTo="opacity-0 scale-95"
                >
                    <div className="mx-2 p-2 md:p-4 bg-red-jss rounded shadow-md text-justify text-white grid grid-cols-12 justify-between items-center">
                        <div className="col-span-11">
                            {flash.alert}
                        </div>
                        <div className="col-span-1 justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" onClick={() => { setVisible(false) }} fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="mx-auto w-6 h-6 cursor-pointer">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                    </div>
                </Transition>
            </div>
            )
        }
    }
};
