import React from "react";
export default function Select({
    name,
    titleOption = "Select One",
    className,
    value,
    handleChange,
    errors = [],
    children,
    ...props
}) {

    return (
        <select name={name} value={value} className={`w-full bg-white mt-1 border border-black ${className}`} onChange={handleChange}{...props}>
            {/* <option value=''>{titleOption}</option> */}
            {children}
        </select>
    )
};
