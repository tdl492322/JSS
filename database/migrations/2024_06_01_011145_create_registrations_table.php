<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('speciment_id');
            $table->foreign('speciment_id')->references('id')->on('speciments')->onDelete('cascade');
            $table->string('source')->default('Local Registration');
            $table->string('ticket_no')->unique();
            $table->string('pnr')->unique();
            $table->string('code')->unique();
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('region_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('company')->nullable();
            $table->string('title')->nullable();
            $table->string('fair_name')->nullable()->default('Jakarta Surface Show');
            $table->string('price')->nullable()->default('ÜCRETSİZ');
            $table->integer('print')->default(0);
            $table->string('status')->default('Active');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('registrations');
    }
};
