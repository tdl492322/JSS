<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('speciments', function (Blueprint $table) {
            $table->id();
            $table->string('value');
            $table->string('label');
            $table->string('image')->nullable();
            $table->string('ratio_photo')->default('square');
            $table->longText('style_single')->nullable();
            $table->longText('style_bulk')->nullable();
            $table->integer('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('speciments');
    }
};
