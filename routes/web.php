<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProfileController;
use App\Models\Registration;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Registration');
});

Route::post('registration', [IndexController::class, 'registration']);
Route::get('registration-success/{pnr}', [IndexController::class, 'registrationSuccess']);
Route::get('print/eticket/{registration_code}', [IndexController::class, 'printEticket']);

Route::get('press-registration', [IndexController::class, 'pressRegistration']);
Route::get('press-register', [IndexController::class, 'postPressRegister']);

Route::get('view', function () {
    // $pnr = Registration::find(1);
    // return view('mails.success', compact('pnr'));
});

Route::prefix('dashboard')->middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/exhibitor', [DashboardController::class, 'exhibitor']);
    Route::get('/press', [DashboardController::class, 'press']);
    Route::get('/speaker', [DashboardController::class, 'speaker']);
    Route::get('/vip', [DashboardController::class, 'vip']);
    Route::get('/visitor', [DashboardController::class, 'visitor']);
    Route::get('/organizer', [DashboardController::class, 'organizer']);
    Route::get('add-new', [DashboardController::class, 'addNew']);
    Route::get('report-scan', [DashboardController::class, 'reportScanner']);

    Route::get('fetch-exhibitor', [DashboardController::class, 'fetchExhibitor']);
    Route::get('fetch-press', [DashboardController::class, 'fetchPress']);
    Route::get('fetch-speaker', [DashboardController::class, 'fetchSpeaker']);
    Route::get('fetch-vip', [DashboardController::class, 'fetchVip']);
    Route::get('fetch-visitor', [DashboardController::class, 'fetchVisitor']);
    Route::get('fetch-organizer', [DashboardController::class, 'fetchOrganizer']);
    Route::get('fetch-scan', [DashboardController::class, 'fetchScan']);

    Route::post('upload-template', [DashboardController::class, 'uploadTemplate']);
    Route::post('mass-action', [DashboardController::class, 'massAction']);
    Route::post('create-new', [DashboardController::class, 'createNew']);
});
Route::get('fetch-register', [DashboardController::class, 'fetchRegister'])->middleware('auth');

Route::get('download-template', [DashboardController::class, 'downloadTemplate'])->middleware('auth');
Route::get('download-template-badge', [DashboardController::class, 'downloadTemplateBadge'])->middleware('auth');
Route::post('upload-data', [DashboardController::class, "uploadData"])->middleware('auth');
Route::get('print', [IndexController::class, 'print']);

Route::get('scan', [IndexController::class, 'scan']);
Route::post('scanner', [IndexController::class, 'postScan']);
// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

require __DIR__ . '/auth.php';
