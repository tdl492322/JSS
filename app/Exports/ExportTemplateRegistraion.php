<?php

namespace App\Exports;

use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportTemplateRegistraion implements
    WithColumnFormatting,
    WithHeadings,
    ShouldAutoSize,
    WithStyles,
    ShouldQueue
{
    use Exportable;
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true, 'size' => 20]],

            // // Styling a specific cell by coordinate.
            // 'B2' => ['font' => ['italic' => true]],

            // // Styling an entire column.
            // 'C' => ['font' => ['size' => 16]],
        ];
    }
    public function headings(): array
    {
        return [
            'name',
            'country',
            'city',
            'mail',
            'phone',
            'company',
            'title',
            'fair_name',
            'ticket_price',
            'code',
            'pnr',
            'creation_date',
        ];
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'I' => NumberFormat::FORMAT_CURRENCY_EUR_INTEGER,
            // 'H' => NumberFormat::FORMAT_CURRENCY_IDR_INTEGER,
        ];
    }
}
