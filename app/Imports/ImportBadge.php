<?php

namespace App\Imports;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use App\Http\Traits\Barcode;
use App\Models\Registration;
use App\Models\Speciment;

class ImportBadge implements
    ShouldQueue,
    SkipsEmptyRows,
    SkipsOnError,
    SkipsOnFailure,
    ToModel,
    WithBatchInserts,
    WithChunkReading,
    WithEvents,
    WithHeadingRow
{
    use Barcode, Importable, SkipsErrors;
    /**
     * @param Collection $collection
     */

    public function model(array $row)
    {
        if (isset($row['speciment'])) {
            $speciment = Speciment::where("value", "like", "%" . $row['speciment'] . '%')->first();

            $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
            $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
            $stateTicketNo = false;
            while (!$stateTicketNo) {
                if (!$checkTicketNo) {
                    $ticketNo = $ticketNo;
                    $stateTicketNo = true;
                } else {
                    $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
                    $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
                    $stateTicketNo = false;
                }
            }

            $pnrNo = $speciment->id . "-20" . $this->num(4);
            $checkPnrNo = Registration::wherePnr($pnrNo)->first();
            $statePnrNo = false;
            while (!$statePnrNo) {
                if (!$checkPnrNo) {
                    $pnrNo = $pnrNo;
                    $statePnrNo = true;
                } else {
                    $pnrNo = $speciment->id . "-20" . $this->num(4);
                    $checkPnrNo = Registration::wherePnr($pnrNo)->first();
                    $statePnrNo = false;
                }
            }

            $codeNo = $speciment->id . "-20" . $this->num(4);
            $checkCodeNo = Registration::whereCode($codeNo)->first();
            $stateCodeNo = false;
            while (!$stateCodeNo) {
                if (!$checkCodeNo) {
                    $codeNo = $codeNo;
                    $stateCodeNo = true;
                } else {
                    $codeNo = $speciment->id . "-20" . $this->num(4);
                    $checkCodeNo = Registration::whereCode($codeNo)->first();
                    $stateCodeNo = false;
                }
            }
            $registration = Registration::create([
                'speciment_id' => $speciment->id,
                'source' => 'Imported Data',
                'ticket_no' => $ticketNo,
                'pnr' => $pnrNo,
                'code' => $codeNo,
                'name' => $row['name'],
                // 'surname' => $row['code'],
                'country' => $row['country'] ?? "",
                // 'city' => $row['city'] ?? "",
                // 'region_code' => $row['code'],
                // 'phone' => $row['phone'] ?? "",
                // 'email' => $row['mail'] ?? "",
                'company' => $row['company'] ?? "",
                'title' => $row['job_title'] ?? "",
            ]);
        }
    }
    public function chunkSize(): int
    {
        // ini untuk mencegah penggunaan memory berlebih
        return 200;
    }
    public function batchSize(): int
    {
        // ini menentukan berapa banyak data dimasukan ke database per 1 batch
        return 1;
    }
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                // Mail::to(auth()->user()->email)->send(new GlobalEventImportGlobalEventHasFailed($this->user, $event));
                // $this->user->notify(new ImportGlobalEventHasFailed($this->user, $event));
                // Notification::send(auth()->user(), new ImportGlobalEventHasFailed(auth()->user(), $event));
            },
        ];
    }
    function onError(\Throwable $e)
    {
        // Mail::to($this->user->email)->send(new GlobalEventImportGlobalEventHasFailed($this->user, $e));
    }
    function onFailure(Failure ...$failures)
    {
        // Mail::to($this->user->email)->send(new GlobalEventImportGlobalEventHasFailed($this->user, $failures));
    }
}
