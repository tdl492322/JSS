<?php

namespace App\Http\Controllers;

use App\Exports\ExportTemplateRegistraion;
use App\Exports\TemplateBadge;
use App\Imports\ImportBadge;
use App\Imports\ImportRegistration;
use App\Models\Registration;
use App\Models\Speciment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Traits\Barcode;
use App\Models\LogScan;

class DashboardController extends Controller
{
    use Barcode;
    function index(): Response
    {
        $swrUrl = url('fetch-register');
        return Inertia::render('Dashboard/Index', compact('swrUrl'));
    }
    function fetchRegister(): JsonResponse
    {
        $registrant = Registration::orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function downloadTemplate()
    {
        return Excel::download(new ExportTemplateRegistraion, 'export-data-' . date("YmdHis") . '.xlsx');
    }
    function uploadData(Request $request)
    {
        $request->validate([
            'template' => 'required'
        ]);
        $renameFile = 'registrant-excel-' . date('YmdHis') . '.' . $request->template->getClientOriginalExtension();
        $request->template->storeAs('/public/excel/', $renameFile);

        Excel::import(new ImportRegistration(), storage_path('/app/public/excel/' . $renameFile));
        return redirect('dashboard/')->with('message', 'Importing data from excel is under progress');
    }
    function exhibitor(): Response
    {
        $swrUrl = url('dashboard/fetch-exhibitor');
        return Inertia::render('Dashboard/Exhibitor', compact('swrUrl'));
    }
    function fetchExhibitor(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(1)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function press(): Response
    {
        $swrUrl = url('dashboard/fetch-press');
        return Inertia::render('Dashboard/Press', compact('swrUrl'));
    }
    function fetchPress(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(5)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function speaker(): Response
    {
        $swrUrl = url('dashboard/fetch-speaker');
        return Inertia::render('Dashboard/Speaker', compact('swrUrl'));
    }
    function fetchSpeaker(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(4)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function vip(): Response
    {
        $swrUrl = url('dashboard/fetch-vip');
        return Inertia::render('Dashboard/Vip', compact('swrUrl'));
    }
    function fetchVip(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(2)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function visitor(): Response
    {
        $swrUrl = url('dashboard/fetch-visitor');
        return Inertia::render('Dashboard/Visitor', compact('swrUrl'));
    }
    function fetchVisitor(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(3)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }
    function organizer(): Response
    {
        $swrUrl = url('dashboard/fetch-organizer');
        return Inertia::render('Dashboard/Organizer', compact('swrUrl'));
    }
    function fetchOrganizer(): JsonResponse
    {
        $registrant = Registration::whereSpecimentId(6)->orderByDesc('created_at')->with(['specimentDetail'])->get();
        return response()->json(compact('registrant'), 200);
    }

    function downloadTemplateBadge()
    {
        return Excel::download(new TemplateBadge, 'template-badge-' . date("YmdHis") . '.xlsx');
    }
    function uploadTemplate(Request $request)
    {
        $request->validate([
            'template' => 'required'
        ]);
        $renameFile = 'badge-excel-' . date('YmdHis') . '.' . $request->template->getClientOriginalExtension();
        $request->template->storeAs('/public/excel/', $renameFile);

        Excel::import(new ImportBadge(), storage_path('/app/public/excel/' . $renameFile));
        return redirect('dashboard/')->with('message', 'Importing data from excel is under progress');
    }
    function massAction(Request $request): JsonResponse
    {
        $request->validate([
            "action" => "required",
            'selected' => "required|array"
        ]);
        $action = $request->action;
        if ($action == 'Print') {
            $registrants = Registration::whereIn('id', $request->selected)->get();
            $selected = array();
            foreach ($registrants as $key => $value) {
                array_push($selected, $value->id);
            }
            $query = http_build_query(array('id' => $selected));
            return response()->json(['url' => url('print?' . $query)]);
        }
    }
    function addNew(): Response
    {
        $speciments = Speciment::all();
        $selectedSpeciment = Speciment::whereLabel(request()->speciment)->first();
        return Inertia::render('AddNew', compact('selectedSpeciment', 'speciments'));
    }
    function createNew(Request $request)
    {
        $request->validate([
            "speciment" => "required|max:255",
            "name" => "required|max:255",
            "title" => "required|max:255",
            "company" => "required|max:255",
            "country" => "required|max:255"
        ]);
        $speciment = Speciment::whereLabel($request->speciment)->first();

        $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
        $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
        $stateTicketNo = false;
        while (!$stateTicketNo) {
            if (!$checkTicketNo) {
                $ticketNo = $ticketNo;
                $stateTicketNo = true;
            } else {
                $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
                $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
                $stateTicketNo = false;
            }
        }

        $pnrNo = $speciment->id . "-20" . $this->num(4);
        $checkPnrNo = Registration::wherePnr($pnrNo)->first();
        $statePnrNo = false;
        while (!$statePnrNo) {
            if (!$checkPnrNo) {
                $pnrNo = $pnrNo;
                $statePnrNo = true;
            } else {
                $pnrNo = $speciment->id . "-20" . $this->num(4);
                $checkPnrNo = Registration::wherePnr($pnrNo)->first();
                $statePnrNo = false;
            }
        }

        $codeNo = $speciment->id . "-20" . $this->num(4);
        $checkCodeNo = Registration::whereCode($codeNo)->first();
        $stateCodeNo = false;
        while (!$stateCodeNo) {
            if (!$checkCodeNo) {
                $codeNo = $codeNo;
                $stateCodeNo = true;
            } else {
                $codeNo = $speciment->id . "-20" . $this->num(4);
                $checkCodeNo = Registration::whereCode($codeNo)->first();
                $stateCodeNo = false;
            }
        }
        $registration = Registration::create([
            'speciment_id' => $speciment->id,
            'source' => 'Single Add Data',
            'ticket_no' => $ticketNo,
            'pnr' => $pnrNo,
            'code' => $codeNo,
            'name' => $request->name,
            'country' => $request->country,
            'company' => $request->company,
            'title' => $request->title,
        ]);
        $selected = array();
        array_push($selected, $registration->id);
        $query = http_build_query(array('id' => $selected));
        return Inertia::location(url('/print?' . $query));
    }
    function reportScanner(): Response
    {
        $swrUrl = url('/dashboard/fetch-scan');
        return Inertia::render('Dashboard/ReportScanner', compact('swrUrl'));
    }
    function fetchScan(): JsonResponse
    {
        $scans = request()->date ? LogScan::whereDate('scan_in', request()->date)->with(['detailScanned'])->get() : LogScan::with(['detailScanned'])->get();
        return response()->json(compact('scans'), 200);
    }
}
