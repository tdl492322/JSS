<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use App\Http\Traits\Barcode;
use App\Models\LogScan;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class IndexController extends Controller
{
    use Barcode;
    function registration(Request $request): RedirectResponse
    {
        $request->validate([
            "name" => "required|max:255",
            "surname" => "required|max:255",
            "country" => "required|max:255",
            "city" => "required|max:255",
            "region_code" => "required|max:255",
            "phone_no" => "required|max:255",
            "email_address" => "required|max:255",
            "company_name" => "required|max:255",
            "title" => "required|max:255",
        ]);

        $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
        $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
        $stateTicketNo = false;
        while (!$stateTicketNo) {
            if (!$checkTicketNo) {
                $ticketNo = $ticketNo;
                $stateTicketNo = true;
            } else {
                $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
                $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
                $stateTicketNo = false;
            }
        }

        $pnrNo = "20" . $this->num(6);
        $checkPnrNo = Registration::wherePnr($pnrNo)->first();
        $statePnrNo = false;
        while (!$statePnrNo) {
            if (!$checkPnrNo) {
                $pnrNo = $pnrNo;
                $statePnrNo = true;
            } else {
                $pnrNo = "20" . $this->num(6);
                $checkPnrNo = Registration::wherePnr($pnrNo)->first();
                $statePnrNo = false;
            }
        }

        $codeNo = "20" . $this->num(6);
        $checkCodeNo = Registration::whereCode($codeNo)->first();
        $stateCodeNo = false;
        while (!$stateCodeNo) {
            if (!$checkCodeNo) {
                $codeNo = $codeNo;
                $stateCodeNo = true;
            } else {
                $codeNo = "20" . $this->num(6);
                $checkCodeNo = Registration::whereCode($codeNo)->first();
                $stateCodeNo = false;
            }
        }
        $registration = Registration::create([
            'speciment_id' => 3,
            'ticket_no' => $ticketNo,
            'pnr' => $pnrNo,
            'code' => $codeNo,
            'name' => $request->name,
            'surname' => $request->surname,
            'country' => $request->country,
            'city' => $request->city,
            'region_code' => $request->region_code,
            'phone' => $request->phone_no,
            'email' => $request->email_address,
            'company' => $request->company_name,
            'title' => $request->title,
        ]);

        try {
            Mail::to($registration->email, $registration->name . ' ' . $registration->surname)->send(new \App\Mail\Registration($registration));
        } catch (\Throwable $th) {
            //throw $th;
        }
        return redirect('registration-success/' . $pnrNo);
    }
    function registrationSuccess($pnr): Response
    {
        $findPnr = Registration::wherePnr($pnr)->first();
        if (!$findPnr) {
            abort(404);
        }
        return Inertia::render('Success', compact('findPnr'));
    }
    function printEticket($registration_code): View
    {
        $pnr = Registration::whereTicketNo($registration_code)->first();
        return view('pdf.ticket', compact('pnr'));
    }
    function print()
    {
        $pnrs = Registration::whereIn('id', request()->id)->with(['specimentDetail'])->get();
        foreach ($pnrs as $pnr) {
            $pnr->update([
                'print' => $pnr->print + 1,
            ]);
        }
        return view('print-badge', compact('pnrs'));
    }
    function scan()
    // : Response
    {
        $location = request()->loc;
        // return $location;
        return Inertia::render('Scan', compact('location'));
    }
    function postScan(Request $request): JsonResponse
    {
        $request->validate(["code" => "required"]);
        $registrant = Registration::whereCode($request->code)->with(["specimentDetail"])->first();
        if (!$registrant) {
            return response()->json([
                "status_code" => 404,
                "message" => "Code not found"
            ]);
        }
        $log = LogScan::create([
            "code" => $registrant->code,
            "speciment" => $registrant->specimentDetail->label,
            "scan_in" => now(),
            "location" => $request->location,
        ]);
        return response()->json([
            "code" => $registrant->code,
            "speciment" => url('assets/images/speciment/' . $registrant->specimentDetail->image),
            "name" => $registrant->name,
            "surname" => $registrant->surname,
            "title" => $registrant->title,
            "company" => $registrant->company,
            "status_code" => 200,
            "message" => "Welcome to Jakarta Surface Show"
        ]);
    }
    function pressRegistration(): Response
    {
        return Inertia::render('PressRegistration');
    }
    function postPressRegister(Request $request): RedirectResponse
    {
        $request->validate([
            "name" => "required|max:255",
            "country" => "required|max:255",
            "company_name" => "required|max:255",
            "title" => "required|max:255",
        ]);

        $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
        $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
        $stateTicketNo = false;
        while (!$stateTicketNo) {
            if (!$checkTicketNo) {
                $ticketNo = $ticketNo;
                $stateTicketNo = true;
            } else {
                $ticketNo = $this->num(4) . $this->lowerCase(2) . $this->num(1) . $this->lowerCase(1) . $this->num(1) . $this->lowerCase(1) . $this->num(3);
                $checkTicketNo = Registration::whereTicketNo($ticketNo)->first();
                $stateTicketNo = false;
            }
        }

        $pnrNo = "20" . $this->num(6);
        $checkPnrNo = Registration::wherePnr($pnrNo)->first();
        $statePnrNo = false;
        while (!$statePnrNo) {
            if (!$checkPnrNo) {
                $pnrNo = $pnrNo;
                $statePnrNo = true;
            } else {
                $pnrNo = "20" . $this->num(6);
                $checkPnrNo = Registration::wherePnr($pnrNo)->first();
                $statePnrNo = false;
            }
        }

        $codeNo = "5-20" . $this->num(4);
        $checkCodeNo = Registration::whereCode($codeNo)->first();
        $stateCodeNo = false;
        while (!$stateCodeNo) {
            if (!$checkCodeNo) {
                $codeNo = $codeNo;
                $stateCodeNo = true;
            } else {
                $codeNo = "5-20" . $this->num(4);
                $checkCodeNo = Registration::whereCode($codeNo)->first();
                $stateCodeNo = false;
            }
        }
        $registration = Registration::create([
            'speciment_id' => 5,
            'ticket_no' => $ticketNo,
            'pnr' => $pnrNo,
            'code' => $codeNo,
            'name' => $request->name,
            'country' => $request->country,
            'company' => $request->company_name,
            'title' => $request->title,
        ]);

        try {
            Mail::to($registration->email, $registration->name . ' ' . $registration->surname)->send(new \App\Mail\Registration($registration));
        } catch (\Throwable $th) {
            //throw $th;
        }
        return redirect('registration-success/' . $pnrNo);
    }
}
