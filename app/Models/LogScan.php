<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LogScan extends Model
{
    use HasFactory;
    protected $fillable = [
        "code",
        "speciment",
        "scan_in",
        "location",
    ];
    function detailScanned(): BelongsTo
    {
        return $this->belongsTo(Registration::class, 'code', 'code');
    }
}
