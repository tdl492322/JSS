<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Registration extends Model
{
    use HasFactory;
    protected $fillable = [
        'speciment_id',
        'source',
        'ticket_no',
        'pnr',
        'code',
        'name',
        'surname',
        'country',
        'city',
        'region_code',
        'phone',
        'email',
        'company',
        'title',
        'fair_name',
        'price',
        'print',
        'status',
        'created_at',
    ];
    function specimentDetail(): BelongsTo
    {
        return $this->belongsTo(Speciment::class, 'speciment_id');
    }
}
