<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciment extends Model
{
    use HasFactory;
    protected $fillable = [
        'value',
        'label',
        'image',
        'ratio_photo',
        'style_single',
        'style_bulk',
        'status',
    ];
}
